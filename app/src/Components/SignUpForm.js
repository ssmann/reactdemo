import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View,TextInput,Text,StyleSheet,Button,Alert} from 'react-native';
import Toast from 'react-native-root-toast';
import {Actions} from 'react-native-router-flux';
class SignUp extends Component
{
  constructor(props) {
     super(props);
     this.state = {firstName: '',lastName:'',email:'',phone:'',city:'',visible:false,errorMessage:'',isFormValid:false};
   }
   handleChange(event) {
      this.setState({firstName: event.target.value});
    }

  onSignupClick(){
    var message = this.state.firstName;
    console.log(this.state.firstName);
    //alert(this.state.firstName)
    setTimeout(() => this.setState({
                visible: false
            }), 2000); // hide toast after 5s
    this.validateForm();
    //Do Sign Up call
    var user={
      email:this.state.email,
      password:"password"
      // lastName:this.state.lastName,
      // firstName:this.state.firstName,
      // city:this.state.city,
      // phone:this.state.phone
    };
    if (this.state.isFormValid) {
      	//alert('JSON='+JSON.stringify(user));
      this.hitRegisterApi(user,"http://54.87.244.231:2078/users/add")
    }
  }

hitRegisterApi(user,url)
{
  alert(user+url)

    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({user})
    })
    .then( (response) => {
    console.log('Received response: ', response);
      //alert("Response"+response);
      return response.json();
    })
      .then( (responseJSON) => {
    console.log('JSON response from API: ', responseJSON);
//alert("JSON Response"+responseJSON);
      // dispatch({
      //   type: ACTION_TYPES.LOGIN_USER,
      //   payload: responseJSON
      // });
      Actions.dashboard();
      })
    .catch(e => {
    alert("error"+e);
    });

    //call the API and use the promise to check the response
    // in response callBack .then() call the dispatcher.

}

validateForm()
{
  if (!this.state.firstName) {
    this.setState({visible:true,errorMessage:"Please enter firstName"})
    return;
  }
  if (!this.state.lastName) {
    this.setState({visible:true,errorMessage:"Please enter lastName"})
    return;
  }
  if (!this.state.email) {
    this.setState({visible:true,errorMessage:"Please enter email"})
    return;
  }
  // if (!this.state.phone) {
  //   this.setState({visible:true,errorMessage:"Please enter valid phone number"})
  //   return;
  // }
  if (!this.state.lastName) {
    this.setState({visible:true,errorMessage:"Please enter lastName"})
    return;
  }
  // if (!this.state.city) {
  //   this.setState({visible:true,errorMessage:"Please enter city"})
  //   return;
  // }
  this.setState({isFormValid:true})
}

  componentDidMount() {
        setTimeout(() => this.setState({
            visible: false
        }), 2000); // hide toast after 5s
    };
  render() {
    console.log('render called');
    return(
      <View style={styles.container}>

      <TextInput 
				style={styles.inputStyle} 
				value={this.state.firstName}
      onChangeText={(text) => this.setState({firstName:text})}
      placeholder='First Name'
				autoCapitalize="none"
				/> 
				<TextInput 
				style={styles.inputStyle} 
				value={this.state.lastNamelastName}
      onChangeText={(text) => this.setState({lastName:text})}
      placeholder='Last Name'
				autoCapitalize="none"
				/> 
				<TextInput 
				style={styles.inputStyle} 
				value={this.state.email}
      onChangeText={(text) => this.setState({email:text})}
      placeholder='Email'
				autoCapitalize="none"
				/> 
				<TextInput 
				style={styles.inputStyle} 
				value={this.state.phone}
      onChangeText={(text) => this.setState({phone:text})}
      placeholder='Phone'
				autoCapitalize="none"
				/> 


      


        <Button  title='Sign up' 			onPress={this.onSignupClick.bind(this)}/>

        <Toast
                visible={this.state.visible}
                position={-20}
                shadow={false}
                animation={false}
                hideOnPress={true}

            >{this.state.errorMessage}</Toast>
      </View>
      )
  }

}
export default SignUp;


const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height:100
  },
  seperator: {
  	height:1,
  	alignSelf: 'stretch',
  	backgroundColor:'#111111',
  	paddingTop:2,
  	marginLeft:20,
  	marginRight:10
  },
  inputStyle: {
    height:30,
    alignSelf: 'stretch',
    paddingLeft:20
  },
  buttonStyle: {
  	height:30,
  	fontSize:40,
  	alignSelf: 'stretch',
  }
});
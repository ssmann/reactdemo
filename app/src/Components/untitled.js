import React, {Component} from 'react';
import {connect} from 'react-redux';
// import Load from 'react-native-loading-gif'
// import Spinner from 'react-native-loading-spinner-overlay';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TextInput,
  Button,
  TouchableOpacity
} from 'react-native';
import {emailChanged,passwordChanged,loginUser} from '../actions/actions'
import {Actions} from 'react-native-router-flux';
//import {Toast} from 'react-native-toast';

import Toast from 'react-native-root-toast';

const { width, height } = Dimensions.get("window");


const background = require("./login1_bg.png");
const mark = require("./login1_mark.png");
const lockIcon = require("./login1_lock.png");
const personIcon = require("./login1_person.png");





class LoginScreen extends Component {

  constructor() {
        super(...arguments);
        this.state = {};
    }
  onEmailChanged(text) {
    console.log('I am here in helper callback function');
    this.props.emailChanged(text);
  }

  onPasswordChanged(text) {
    console.log('Hello');
    this.props.passwordChanged(text);
  }

  validate(values){
    var status=true;

         
      if (!values.email) {
        this.setState({
              email: true
            })
            
        status=false;
      } 

      if (!values.password) {
        this.setState({
              password: true
            })
        status=false;
      }   
      setTimeout(() => this.setState({
            email: false,
            password:false
           }), 2000); // hide toast after 5s  
      console.log(this.state)
      return status;  
    }

  onLoginButtonPress() {
    var user={
      username:this.props.email,
      password:this.props.password
    };

    var data=this.validate(this.props);
    
       console.log(data);
    
    if(data==true){

      console.log('USER='+JSON.stringify(user));
    this.props.loginUser(user);
    }

      
  }

  getUIElement() {
    return (<Button onPress={this.onLoginButtonPress.bind(this)} title='Login'/>);
  }
  
   ChangeState = () => {
    Actions.SignupVriew();
    console.log('clickity');
  }


  render() {
    return (
      <View style={styles.container}>
        <Image source={background} style={styles.background} resizeMode="cover">
          <View style={styles.markWrap}>
            <Image source={mark} style={styles.mark} resizeMode="contain" />
          </View>
          <View style={styles.wrapper}>
            <View style={styles.inputWrap}>
              <View style={styles.iconWrap}>
                <Image source={personIcon} style={styles.icon} resizeMode="contain" />
              </View>
              <TextInput 
                style={styles.input} 
                placeholder='Email id' 
                onChangeText={this.onEmailChanged.bind(this)}
                value={this.props.email}
                  placeholderTextColor="#FFF"
                /> 
             
            </View>
            <View style={styles.inputWrap}>
              <View style={styles.iconWrap}>
                <Image source={lockIcon} style={styles.icon} resizeMode="contain" />
              </View>
              <TextInput 
               onChangeText={this.onPasswordChanged.bind(this)}
               value={this.props.password}
                placeholderTextColor="#FFF"
                placeholder="Password" 
                style={styles.input} 
                secureTextEntry 
              />
            </View>
            <TouchableOpacity activeOpacity={.5}>
              <View>
                <Text style={styles.forgotPasswordText}>Forgot Password?</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={.5} onPress={this.onLoginButtonPress.bind(this)} >
              <View style={styles.button}>
                <Text style={styles.buttonText}>Sign In</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.container}>
            <View style={styles.signupWrap}>
              <Text style={styles.accountText}>Don't have an account?</Text>
              <TouchableOpacity activeOpacity={.5} onPress={this.ChangeState}>
                <View>
                  <Text style={styles.signupLinkText}>Sign Up</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Image>
        <Toast
                visible={this.state.email}
                position={50}
                shadow={false}
                animation={false}
                hideOnPress={true}
            >Please enter the username</Toast>  

            <Toast
                visible={this.state.password}
                position={50}
                shadow={false}
                animation={false}
                hideOnPress={true}
            >Please enter the password</Toast>  
      </View>
    );
  }
}
export default connect(mapStateToProps,{emailChanged,passwordChanged,loginUser})(LoginScreen);
const mapStateToProps = state => {
  return {
    email: state.login.email,
    password:state.login.password,
    accessToken:state.login.accessToken,
    isLoading:state.login.isLoading
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  markWrap: {
    flex: 1,
    paddingVertical: 30,
  },
  mark: {
    width: null,
    height: null,
    flex: 1,
  },
  background: {
    width,
    height,
  },
  wrapper: {
    paddingVertical: 30,
  },
  inputWrap: {
    flexDirection: "row",
    marginVertical: 10,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC"
  },
  iconWrap: {
    paddingHorizontal: 7,
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    height: 20,
    width: 20,
  },
  input: {
    flex: 1,
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: "#FF3366",
    paddingVertical: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  buttonText: {
    color: "#FFF",
    fontSize: 18,
  },
  forgotPasswordText: {
    color: "#D8D8D8",
    backgroundColor: "transparent",
    textAlign: "right",
    paddingRight: 15,
  },
  signupWrap: {
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  accountText: {
    color: "#D8D8D8"
  },
  signupLinkText: {
    color: "#FFF",
    marginLeft: 5,
  }
});

import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    console.log('did mount');
  }
  componentWillMount() {
    
    fetch('http://54.87.244.231:2078/users/userOne/589454dd1893327921ec4fa4', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization' : 'Bearer ' +'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWQiOnsiX2lkIjoiNTg5NDU0ZGQxODkzMzI3OTIxZWM0ZmE0IiwiZW1haWwiOiJyYWplZXZrQHNtYXJ0ZGF0YWluYy5uZXQiLCJ1c2VyX25hbWUiOiJyYWplZXZrIiwicGFzc3dvcmQiOiJmZTNjMzFjMjk2NWEyZmVkMThiZmVjY2Q4YmFhYTk4OSIsIl9fdiI6NDMsImRldmljZV9pZCI6ImY0X0RfTnhFaFdvOkFQQTkxYkZoV09IV0hRbmwxSUNkWWNTd3VZUXc3Z2l6Z0o1SzdjaGlHaGRpTG5hMjNKbFI2R2VrUmJxNkhyazgzMHZjdVlXeG0tQ0VsUEJvdEk3UkFGYTNFRTZLV2dwNzFnbE92R3g3NkY5UjFCQ1VYTUppWkNOdnNYWU1OU3YtaEliS0FmQmRwVkxmIiwicGxhdGVmb3JtIjoiQW5kcm9pZCIsInRpbWV6b25lIjoiLTMzMCIsInNvY2tldGlkIjoiIiwiYWJvdXRfbWUiOiJIYXJkd29ya2VyLi4uLnNzIiwiZmlyc3RfbmFtZSI6IlJhamVldiIsImxhc3RfbmFtZSI6Ikt1c2h3YWgiLCJwaG9uZSI6IjEyMTAxOTEzNDEiLCJ6aXBjb2RlIjoiMTYxNjEiLCJwcm9mX2ltYWdlIjoiNTg5NDU0ZGQxODkzMzI3OTIxZWM0ZmE0XzE0OTI0OTYwMzgyOTIuanBnIiwiaWQiOiI1ODk0NTRkZDE4OTMzMjc5MjFlYzRmYTQiLCJjb3Zlcl9pbWFnZSI6IjU4OTQ1NGRkMTg5MzMyNzkyMWVjNGZhNF8xNDkyNDk2MDUzMjY3LmpwZyIsImxhc3Rfc2VlbiI6IjIwMTctMDQtMThUMTI6NDQ6MjMuMTc0WiIsImRlZmF1bHRTY3JlZW4iOjEsImxvZ2dlZF9zdGF0dXMiOnRydWUsImNvbm5lY3RlZF9hY2NvdW50X3N0YXR1cyI6dHJ1ZSwiY3JlYXRlZF9kYXRlIjoiMjAxNy0wMi0wM1QxMDowMTowMS44NDNaIiwibG9jYXRpb24iOlt7InR5cGUiOiJQb2ludCIsIl9pZCI6IjU4OTQ1NTQ1MTg5MzMyNzkyMWVjNGZhNiIsImNvb3JkaW5hdGVzIjpbMzAuNzA4MTkwNCw3Ni43MDIzMDk3XX1dLCJyYXRpbmciOlt7ImpvYl9hdXRob3JfaWQiOiI1ODk0NTFkNjE4OTMzMjc5MjFlYzRmOWUiLCJqb2JfaWQiOiI1ODk0NTMxYzE4OTMzMjc5MjFlYzRmYTIiLCJvZmZlcl9pZCI6IjU4OTQ1NjM3MTg5MzMyNzkyMWVjNGZhOSIsInJldmlldyI6Ikdvb2Qgd29yayIsIm1heCI6NSwidmFsdWUiOjUsIl9pZCI6IjU4OTQ1NmUyMTg5MzMyNzkyMWVjNGZhZCIsImNyZWF0ZWRfZGF0ZSI6IjIwMTctMDItMDNUMTA6MDk6MzguNjIzWiJ9LHsiam9iX2F1dGhvcl9pZCI6IjU4OTQ1MWQ2MTg5MzMyNzkyMWVjNGY5ZSIsImpvYl9pZCI6IjU4Yjk5NWQ5ODAzYjJkOWI3ZTg0ZDQ2YSIsIm9mZmVyX2lkIjoiNThiYTQ5N2ZmMjNlZTIyMjA3NWQ4MjRmIiwicmV2aWV3IjoiZ29vZCB3b3JrIiwibWF4Ijo1LCJ2YWx1ZSI6NCwiX2lkIjoiNThiYTRhYTJmMjNlZTIyMjA3NWQ4MjU5IiwiY3JlYXRlZF9kYXRlIjoiMjAxNy0wMy0wNFQwNTowMzozMC45ODVaIn1dLCJza2lsbCI6WyI1N2I0ODNiNjQxMDQ1MGM4MDVjZmE1NDIiLCI1N2I1NDM1OTQxMDQ1MGM4MDVjZmE1NDkiLCI1N2I1NDRlNjQxMDQ1MGM4MDVjZmE1NGEiLCI1N2JhOTJiNzEyZWNlNDVlMGI5ZWMxZTciLCI1N2JhOTJkYTEyZWNlNDVlMGI5ZWMxZTgiLCI1N2JhOTMxMDEyZWNlNDVlMGI5ZWMxZWEiLCI1N2JhOTMzMjEyZWNlNDVlMGI5ZWMxZWIiLCI1N2JhOTM0MjEyZWNlNDVlMGI5ZWMxZWMiXSwiaXNfZGVsZXRlZCI6ZmFsc2UsInN0YXR1cyI6dHJ1ZSwiZW5hYmxlIjp0cnVlLCJyb2xlIjpbdHJ1ZSxmYWxzZV19LCJpYXQiOjE0OTI1MTk0NzR9.mlImFn9ZuEQggOdfcEcCGuzge68t5hz1iR9jy-SQGJ8'
      }
      
    })
    .then( (response) => {
      console.log('Received response: ', response);
      return response.json();
    })
      .then( (responseJSON) => {
      console.log('JSON response from API: ', responseJSON);
 
    //  Actions.dashboard();
      })
    .catch(e => {
      console.log('Error==='+e);
    });
  }
  render() {
    console.log('render from dashboard');
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.ios.js
        </Text>
        <Text style={styles.instructions}>
          Press Cmd+R to reload,{'\n'}
          Cmd+D or shake for dev menu
        </Text>
      </View>
      );
  }
	
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    height:200
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


import React, {Component} from 'react';
import {connect} from 'react-redux';
// import Load from 'react-native-loading-gif'
// import Spinner from 'react-native-loading-spinner-overlay';
import {View,TextInput,Text,StyleSheet,Button} from 'react-native';
import {emailChanged,passwordChanged,loginUser} from '../actions/actions'
import {Actions} from 'react-native-router-flux';

class PostJob extends Component {

	
	
	
	render() {
		console.log('render called');
		return(
			<View style={styles.container}>
				<TextInput 
				style={styles.inputStyle} 
				placeholder='Email id' 
				
				/> 

				<View style={styles.seperator}/>

				<TextInput 
				style={styles.inputStyle} 
				placeholder='Password' 
				
				/> 
				<Button  title='Post job'/>
		
			</View>
			)
	}
}



export default connect(null,{})(PostJob);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height:100
  },
  seperator: {
  	height:1,
  	alignSelf: 'stretch',
  	backgroundColor:'#111111',
  	paddingTop:2,
  	marginLeft:20,
  	marginRight:10
  },
  inputStyle: {
    height:30,
    alignSelf: 'stretch',
    paddingLeft:20
  },
  buttonStyle: {
  	height:30,
  	fontSize:40,
  	alignSelf: 'stretch',
  }
});

import ACTION_TYPES from './ActionTypes';
import {Actions} from 'react-native-router-flux';
import {View,TextInput,Text,StyleSheet,Alert} from 'react-native';

export const emailChanged = (text) => {

	return {
		type: ACTION_TYPES.EMAIL_CHANGED,
		payload: text
	}
};


export const passwordChanged = (text) => {

	return {
		type: ACTION_TYPES.PASSWORD_CHANGED,
		payload: text
	}
};






export const loginUser = ({username,password}) => {
	console.log('Login button action received');
	console.log('JSON='+JSON.stringify({username,password}));
	return (dispatch) => {
		fetch('http://54.87.244.231:2078/users/authenticate', {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json'
		   
		  },
		  body: JSON.stringify({username,password})
		})
		.then( (response) => {
			console.log('Received response: ', response);
            
            return response.json();
		})
	    .then( (responseJSON) => {
			console.log('JSON response from API: ', JSON.stringify(responseJSON.messageId));

			
   if(responseJSON.messageId==200){
   	dispatch({
				type: ACTION_TYPES.LOGIN_USER,
				payload: responseJSON
			});
			
			Actions.dashboard();

   }else{

   	Alert.alert(
				'Login credential is invalid!',
				'Either email is incorrect or password,please try again!',
				[
					{text: 'Ok'}
					
				]
          )

   }

			
	    })
		.catch(e => {
			console.log('Error==='+e);
		});

		//call the API and use the promise to check the response
		// in response callBack .then() call the dispatcher.
	}


};
import React from 'react';
import { View } from 'react-native';
import {Provider} from 'react-redux';
import {createStore,applyMiddleware} from 'redux';
import reducers from './reducers/reducers.js'
import ReduxThunk from 'redux-thunk';

import Router from './Router';

const App = () => {
	return (
			<Provider store={createStore(reducers,{},applyMiddleware(ReduxThunk))}>
				
				<Router/>
			</Provider>
			
		);
}

export default App;
import ACTION_TYPES from '../actions/ActionTypes';
const INITIAL_STATE = {email:'',password:'',accessToken:''}

const LoginReducer = (state = INITIAL_STATE,action) => {
	// console.log('TYPE==='+INITIAL_STATE);
	switch (action.type) {
		case ACTION_TYPES.EMAIL_CHANGED:
			// state.email = action.payload;
			// return state
			console.log('action.payload=='+action.payload);
			return {...state,'email':action.payload}
		case ACTION_TYPES.PASSWORD_CHANGED:
			console.log('PASSWORD action.payload=='+action.payload);
			return {...state,'password':action.payload}
		case ACTION_TYPES.LOGIN_USER:
			console.log('LOGIN_USER action.payload=='+action.payload);
			return {...state,'accessToken':action.payload}
		default:
			return state;
	}
};

export default LoginReducer;

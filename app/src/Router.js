import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import LoginForm from './Components/LoginForm'
import Dashboard from './Components/Dashboard'
import SignUpForm from './Components/SignUpForm'
import PostJob from './Components/PostJob'
import ProductsList from './Components/ProductsList'

const RouterComponent = () => {
	return (
		<Router sceneStyle = {{paddingTop: 60}}>
          <Scene key="dashboard" component={Dashboard} title="Dashboard"/>
		  <Scene key="login" component={LoginForm} title="Login" initial/>
		 <Scene key="signup" component={SignUpForm} title="Signup" />
		 <Scene key="postjob" component={PostJob} title="Postjob" />
		 </Router>
		);
};

export default RouterComponent;
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {AppRegistry} from 'react-native';
import App from './app/src/app';


AppRegistry.registerComponent('mockApp', () => App);
